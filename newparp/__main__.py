import sys
import traceback

from newparp import app

if "--debug" in sys.argv:
    import inspect
    import pprint
    app.debug = True

try:
    app.run(threaded=True)
except Exception as e:
    print("newparp app crashed!")
    print("{a}: {b}".format(a=type(e).__name__, b=str(e)))
    traceback.print_tb(e.__traceback__, limit=None)
    if "--debug" in sys.argv:
        print("performing inspection...")
        frames = inspect.getinnerframes(tb=e.__traceback__)
        for frame in frames:
            print("".zfill(80).replace("0", "-"))
            print(frame.function)
            print()
            print("args")
            pprint.pprint(inspect.getargvalues(frame.frame).args)
            print("varargs")
            pprint.pprint(inspect.getargvalues(frame.frame).varargs)
            print("keywords")
            pprint.pprint(inspect.getargvalues(frame.frame).keywords)
            print("locals")
            pprint.pprint(inspect.getargvalues(frame.frame).locals)
    sys.exit(1)

