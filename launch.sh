#!/usr/bin/env bash
if [[ -n "${MXRP_ENTRYPOINT}" ]]; then
    echo "Evaluating MXRP_ENTRYPOINT"
    eval "${MXRP_ENTRYPOINT}" 2>&1
else
    echo "MXRP_ENTRYPOINT undefined!"
fi