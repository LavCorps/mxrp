-- /.gitlab-ci.dhall
let GitLab =
    --TODO: Consider replacing with a fork for better versioning
      https://raw.githubusercontent.com/bgamari/dhall-gitlab-ci/eaa32660c024b3205dc3a32da3d1990243d61870/package.dhall

let RepoURL = "registry.gitlab.com/lavcorps/mxrp"

let ImgURL =
      "registry.gitlab.com/lavcorps-line/infrastructure/utility-dockers/img:stable"

let Prelude =
      https://raw.githubusercontent.com/dhall-lang/dhall-lang/v21.1.0/Prelude/package.dhall
        sha256:0fed19a88330e9a8a3fbe1e8442aa11d12e38da51eb12ba8bcb56f3c25d0854a

let Job = GitLab.Job.Type

let JobType = { latest = 0, stable = 1, scheduled = 2 }

let Target
    : Type
    = { name : Text, commands : List Text, stable : Bool }

let Targets =
      [ { name = "mxrp"
        , commands = [] : List Text
        , stable = True
        }
      ]

let DefaultTestCommands = [ "echo \"Hello World!\"" ]

let BuildCommands =
      { latest =
        [ ''
          printf "\n$COLOR_F_MAGENTA_D Building Latest Docker Images...$COLOR_F_DEFAULT\n"
          img -b native login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
          if [ "$CI_COMMIT_BRANCH" = "$CI_DEFAULT_BRANCH" ]; then
              img -b native build -t "$CI_REGISTRY_IMAGE/$buildVer:latest" .
              img -b native push "$CI_REGISTRY_IMAGE/$buildVer:latest"
          fi
          img -b native build -t "$CI_REGISTRY_IMAGE/$buildVer:commit-$CI_COMMIT_SHORT_SHA" .
          img -b native push "$CI_REGISTRY_IMAGE/$buildVer:commit-$CI_COMMIT_SHORT_SHA"
          img -b native prune
          printf "\n$COLOR_F_GREEN_D Done!...$COLOR_F_DEFAULT\n"
          ''
        ]
      , stable =
        [ ''
          printf "\n$COLOR_F_MAGENTA_DBuilding Stable Docker Images...$COLOR_F_DEFAULT\n"
          img -b native login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
          img -b native build -t "$CI_REGISTRY_IMAGE/$buildVer:stable" .
          img -b native push "$CI_REGISTRY_IMAGE/$buildVer:stable"
          img -b native build -t "$CI_REGISTRY_IMAGE/$buildVer:release-$OG_COMMIT_TAG" .
          img -b native push "$CI_REGISTRY_IMAGE/$buildVer:release-$OG_COMMIT_TAG"
          img -b native prune
          printf "\n$COLOR_F_GREEN_D Done!...$COLOR_F_DEFAULT\n"
          ''
        ]
      , scheduled =
        [ ''
          printf "\n$COLOR_F_MAGENTA_D Performing Scheduled Docker Image Build...$COLOR_F_DEFAULT\n"
          git config --global --add safe.directory $CI_PROJECT_DIR
          git checkout $(git describe --tags $(git rev-list --tags --max-count=1))
          if [ ! -n "$OG_COMMIT_TAG" ]; then
              export OG_COMMIT_TAG=$(git describe --tags $(git rev-list --tags --max-count=1))
          fi
          img -b native login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
          img -b native build -t "$CI_REGISTRY_IMAGE/$buildVer:stable" .
          img -b native push "$CI_REGISTRY_IMAGE/$buildVer:stable"
          img -b native build -t "$CI_REGISTRY_IMAGE/$buildVer:release-$OG_COMMIT_TAG" .
          img -b native push "$CI_REGISTRY_IMAGE/$buildVer:release-$OG_COMMIT_TAG"
          img -b native prune
          printf "\n$COLOR_F_GREEN_D Done!...$COLOR_F_DEFAULT\n"
          ''
        ]
      }

let MkJob
    : forall (type : Natural) ->
      forall (build : Bool) ->
      forall (target : Target) ->
        Job
    = \(type : Natural) ->
      \(build : Bool) ->
      \(target : Target) ->
        GitLab.Job::{
        , stage = Some (if build then "build" else "test")
        , image = Some
            ( if    build
              then  GitLab.Image::{ name = ImgURL, entrypoint = Some [ "" ] }
              else  if Prelude.Natural.equal type JobType.latest
              then  GitLab.Image::{
                    , name =
                        "${RepoURL}/${target.name}:commit-\$CI_COMMIT_SHORT_SHA"
                    , entrypoint = Some [ "" ]
                    }
              else  if Prelude.Natural.equal type JobType.stable
              then  GitLab.Image::{
                    , name =
                        "${RepoURL}/${target.name}:release-\${OG_COMMIT_TAG}"
                    , entrypoint = Some [ "" ]
                    }
              else  GitLab.Image::{
                    , name = "${RepoURL}/${target.name}:stable"
                    , entrypoint = Some [ "" ]
                    }
            )
        , script =
            if    build
            then  if    Prelude.Natural.equal type JobType.latest
                  then  BuildCommands.latest
                  else  if Prelude.Natural.equal type JobType.stable
                  then  BuildCommands.stable
                  else  BuildCommands.scheduled
            else  DefaultTestCommands # target.commands
        , variables = toMap { buildVer = target.name }
        , needs =
            if    build
            then  [] : List Text
            else  [ Prelude.Text.concatSep
                      " "
                      [ "build"
                      , if    Prelude.Natural.equal type JobType.latest
                        then  "latest"
                        else  if Prelude.Natural.equal type JobType.stable
                        then  "stable"
                        else  "scheduled"
                      , target.name
                      ]
                  ]
        , rules = Some
          [ GitLab.Rule::{
            , `if` = Some
                ( if    Prelude.Natural.equal type JobType.latest
                  then  "\$OG_COMMIT_TAG == \"\" && \$OG_PIPELINE_SOURCE != \"schedule\""
                  else  if Prelude.Natural.equal type JobType.stable
                  then  "\$OG_COMMIT_TAG != \"\" && \$OG_PIPELINE_SOURCE != \"schedule\""
                  else  "\$OG_PIPELINE_SOURCE == \"schedule\""
                )
            }
          ]
        }

let MkJobRecord
    : forall (type : Natural) ->
      forall (build : Bool) ->
      forall (target : Target) ->
        { mapKey : Text, mapValue : Optional Job }
    = \(type : Natural) ->
      \(build : Bool) ->
      \(target : Target) ->
        if    target.stable
        then  { mapKey =
                  Prelude.Text.concatSep
                    " "
                    [ if build then "build" else "test"
                    , if    Prelude.Natural.equal type JobType.latest
                      then  "latest"
                      else  if Prelude.Natural.equal type JobType.stable
                      then  "stable"
                      else  "scheduled"
                    , target.name
                    ]
              , mapValue = Some (MkJob type build target)
              }
        else  if Prelude.Natural.equal type JobType.latest
        then  { mapKey =
                  Prelude.Text.concatSep
                    " "
                    [ if build then "build" else "test"
                    , if    Prelude.Natural.equal type JobType.latest
                      then  "latest"
                      else  if Prelude.Natural.equal type JobType.stable
                      then  "stable"
                      else  "scheduled"
                    , target.name
                    ]
              , mapValue = Some (MkJob type build target)
              }
        else  { mapKey =
                  Prelude.Text.concatSep
                    " "
                    [ if build then "build" else "test"
                    , if    Prelude.Natural.equal type JobType.latest
                      then  "latest"
                      else  if Prelude.Natural.equal type JobType.stable
                      then  "stable"
                      else  "scheduled"
                    , target.name
                    ]
              , mapValue = None Job
              }

let Jobs
    : List { mapKey : Text, mapValue : Job }
    =   Prelude.Map.unpackOptionals
          Text
          Job
          ( Prelude.List.map
              Target
              { mapKey : Text, mapValue : Optional Job }
              (MkJobRecord JobType.latest True)
              Targets
          )
      # Prelude.Map.unpackOptionals
          Text
          Job
          ( Prelude.List.map
              Target
              { mapKey : Text, mapValue : Optional Job }
              (MkJobRecord JobType.stable True)
              Targets
          )
      # Prelude.Map.unpackOptionals
          Text
          Job
          ( Prelude.List.map
              Target
              { mapKey : Text, mapValue : Optional Job }
              (MkJobRecord JobType.scheduled True)
              Targets
          )
      # Prelude.Map.unpackOptionals
          Text
          Job
          ( Prelude.List.map
              Target
              { mapKey : Text, mapValue : Optional Job }
              (MkJobRecord JobType.latest False)
              Targets
          )
      # Prelude.Map.unpackOptionals
          Text
          Job
          ( Prelude.List.map
              Target
              { mapKey : Text, mapValue : Optional Job }
              (MkJobRecord JobType.stable False)
              Targets
          )
      # Prelude.Map.unpackOptionals
          Text
          Job
          ( Prelude.List.map
              Target
              { mapKey : Text, mapValue : Optional Job }
              (MkJobRecord JobType.scheduled False)
              Targets
          )

in  Prelude.JSON.renderYAML
      ( GitLab.Top.toJSON
          GitLab.Top::{ jobs = Jobs, stages = Some [ "build", "test" ] }
      )
